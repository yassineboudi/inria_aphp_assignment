import pandas as pd
import numpy as np
from fuzzywuzzy import fuzz
from pandas._testing import assert_frame_equal
import pytest


def same_phone(r1, r2):
    """Tester si le numéro de téléphone est identique dans les deux records.

    Args:
        param1 (record): r1.
        param2 ((record): r2.

    Returns:
        bool: True pour identique, False autrement.

    """
    return r1['phone_number'] == r2['phone_number']


def same_post_code(r1, r2):
    """Tester si le code postale est identique dans les deux records.

    Args:
        param1 (record): r1.
        param2 ((record): r2.

    Returns:
        bool: True pour identique , False autrement.

    """
    return r1['postcode'] == r2['postcode']


def similar_address(r1, r2):
    """Tester si l'adresse est similair dans les deux records.
    similarité totale (55) ou partiel (75)
    adresse = street_number + address_1 + suburb + address_2
    Args:
        param1 (record): r1.
        param2 ((record): r2.

    Returns:
        bool: True pour identique , False autrement.

    """
    adr_r1 = str(r1['street_number'])+" " + \
        r1['address_1'] + " " + r1['suburb']
    adr_r2 = str(r2['street_number'])+" " + \
        r2['address_1'] + " " + r2['suburb']
    return (
        fuzz.ratio(adr_r1, adr_r2) > 55 or
        fuzz.partial_ratio(adr_r1, adr_r2) > 75
    )


def similar_name(r1, r2):
    """Tester si le nom+prénom est similair(proche de 80) dans les deux records.

    Args:
        param1 (record): r1.
        param2 ((record): r2.

    Returns:
        bool: True pour identique , False autrement.

    """
    r1_name = r1['given_name']+" " + r1['surname']
    r2_name = r2['given_name']+" " + r2['surname']
    return fuzz.partial_ratio(r1_name, r2_name) > 80


def similar_state(r1, r2):
    """Tester si l'état est similair(proche de 80) dans les deux records.

    Args:
        param1 (record): r1.
        param2 ((record): r2.

    Returns:
        bool: True pour identique , False autrement.

    """
    return fuzz.partial_ratio(r1['state'], r2['state']) > 80


def same_patient(r1, r2):
    """Tester la similarité patient dans les deux records avec combinaison de multiple colonne.

    Args:
        param1 (record): r1.
        param2 ((record): r2.

    Returns:
        bool: True pour identique , False autrement.

    """
    return (
        similar_name(r1, r2) and
        similar_state(r1, r2) and
        same_post_code(r1, r2) and
        same_phone(r1, r2) and
        similar_address(r1, r2)

    )


def detect_duplicates(df, match_func, max_size=None, block_by=None):

    def get_record_index(r):
        return r[df.index.name or 'index']

    records = df.to_records()

    duplicates = []

    def find_duplicate(at=0, duplicate=None, indexes=None):

        r1 = records[at]

        if duplicate is None:
            duplicate = {get_record_index(r1)}
            indexes = [at]

        # Stop if enough duplicates have been found
        if max_size is not None and len(duplicate) == max_size:
            return duplicate, indexes

        for i, r2 in enumerate(records):

            if get_record_index(r2) in duplicate or i == at:
                continue

            if match_func(r1, r2):
                duplicate.add(get_record_index(r2))
                indexes.append(i)
                find_duplicate(at=i, duplicate=duplicate, indexes=indexes)

        return duplicate, indexes

    while len(records) > 0:
        duplicate, indexes = find_duplicate()
        duplicates.append(duplicate)
        records = np.delete(records, indexes)

    return pd.Series({
        idx: duplicate_id
        for duplicate_id, idxs in enumerate(duplicates)
        for idx in idxs
    })


def remove_duplicates(file_path):
    df_patient = pd.read_csv(file_path)
    df_patient.dropna(inplace=True)
    df_patient.drop_duplicates(inplace=True)
    df_patient['real_id'] = detect_duplicates(
        df=df_patient,
        match_func=same_patient,
        max_size=2)
    df_patient = df_patient.drop_duplicates(subset=["real_id"])
    df_patient = df_patient.drop(columns=['real_id']).reset_index()
    return df_patient.drop(columns=['index'])


def test_similar_name():

    d_names = {'given_name': ['matissee', 'matisse', 'alice'],
               'surname': ['clarke', 'clarke', 'conboy']
               }
    df = pd.DataFrame(d_names, columns=['given_name', 'surname'])
    records = df.to_records()
    r1 = records[0]
    r2 = records[1]
    sim_res = similar_name(r1, r2)
    assert sim_res == True


def test_similar_address():

    d_address = {'street_number': [13.0, 13.0],
                 'address_1': ['reene street', 'rene street'],
                 'suburb': ['ellenbrook', 'ellenbrok']
                 }
    df = pd.DataFrame(d_address, columns=[
                      'street_number', 'address_1', 'suburb'])
    records = df.to_records()
    r1 = records[0]
    r2 = records[1]
    sim_res = similar_address(r1, r2)
    assert sim_res == True


def test_duplicate_case():
    df_patient_res = remove_duplicates("csv_test/df_patient_test.csv")
    df_patient_expected = pd.read_csv("csv_test/csv_dup.csv")
    assert_frame_equal(df_patient_res, df_patient_expected)
