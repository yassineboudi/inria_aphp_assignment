# COVID-19 Analysis in Australia

## Dépendances

### OS testé:

- Ubuntu 16.04

### Modules:

- fuzzywuzzy

### Instalation:

```

pip install fuzzywuzzy

```

## Structure

    .
    ├── resources                       # Fichiers coordonéees latitude longitude + codes postaux
    │   ├── states_latlong.csv          # Fichier csv contient coordonéees latitude longitude des états de l'Australie
    │   └── au_postcodes.json           # Fichier json contient tous les codes postaux de l'Australie
    ├── maps                            # Fichiers nécessaire pour créer map de l'Australie
    ├── highlights_errors.ipynb          # Notebook pour mettre en évidence les problèmes de qualité de données (Tâche #1)
    ├── detect_duplicates.ipynb          # Notebook pour Suppression des doublons et EDA (Tâche #2 et #4)
    ├── test_duplicates.py              # Script python pour tester detect_duplicates() (Tâche #3)
    ├── csv_test                        # Fichiers nécessaire pour le test
    │   ├── df_patient_test..csv        # Fichier csv "source" utilisé dans le test
    │   └── csv_dup.json                # Fichier csv "résultat" utilisé dans le test
    └── README.md

## Test

```

pytest

```

## Réfrences

## Auteur

Yassine Boudi
